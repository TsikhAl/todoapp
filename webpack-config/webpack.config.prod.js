var common = require('./webpack.config.common.js');
var webpack = require('webpack');
var merge = require('webpack-merge');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
	devtool: 'source-map',
	plugins: [
	 	new webpack.optimize.ModuleConcatenationPlugin(),
	 	new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
		new UglifyJsPlugin({
			sourceMap: true
		})
	]
});