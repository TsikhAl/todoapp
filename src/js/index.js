import '../styles/main.scss';
import {todoFactory} from './services/todoFactory.service.js';
import {toDoController} from './controllers/toDoController.controller';
import {addEditController} from './controllers/addOrEditTodo.controller.js';
import {mainController} from './controllers/main.controller.js';
import {addValidation} from './directives/addValidation.directive.js';

var app = angular.module('toDoApp', ['ngRoute', 'ngMessages']);

app.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		templateUrl: './src/templates/main.html',
		controller: 'mainController'
	})
	.when('/todo/add', {
		templateUrl: './src/templates/add-or-edit-todo.html',
		controller: 'addEditController'
	})
	.when('/todo/edit/:id', {
		templateUrl: './src/templates/add-or-edit-todo.html',
		controller: 'addEditController'
	})
	.otherwise({redirectTo: '/'});

	$locationProvider.html5Mode(true);
});

app.directive('addValidation', addValidation);

app.factory('todoFactory', todoFactory);

app.controller('toDoController', toDoController);

app.controller('mainController', mainController);

app.controller('addEditController', addEditController);