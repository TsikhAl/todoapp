function toDoController($scope, todoFactory, $filter) {
	$scope.todos = todoFactory.getTodos();

	$scope.$watch('todos', function () {
		$scope.remainingTasksCount = $filter('filter')($scope.todos, { completed: false }).length;
		$scope.completedTasksCount = $scope.todos.length - $scope.remainingTasksCount;
	}, true)
}

export {toDoController};
