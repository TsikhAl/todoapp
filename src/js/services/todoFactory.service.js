function todoFactory() {
	var todoList = [
			{
				id: '_7',
				title: 'First item',
				completed: false,
				date: 1521369559773
			},{
				id: '_1',
				title: 'New Delhi',
				completed: false,
				date: 1521110359773
			},{
				id: '_2',
				title: 'Mumbai',
				completed: false,
				date: 1521110359773
			},{
				id: '_4',
				title: 'Chennai',
				completed: false,
				date: 1521196759773
			},{
				id: '_5',
				title: 'Last item',
				completed: false,
				date: 1521283159773
			},{
				id: '_3',
				title: 'Kolkata',
				completed: false,
				date: 1521196759773
			},{
				id: '_6',
				title: 'Kekeke',
				completed: false,
				date: 1521369559773
			}
		];

	return {
		getTodos: function getTodos() {
			 return todoList;
		},
		addTodo: function addTodo(newTodo) {
			todoList.push(newTodo);
		},
		removeTodo: function removeTodo(id) {
			var index = todoList.findIndex(elem => elem.id === id);
			todoList.splice(index, 1);
		},
		changeTodoTitle: function(todoId, newTitle) {
			var index = todoList.findIndex(elem => elem.id === todoId);
			todoList[index].title = newTitle;
		},
		toggleTodo: function checkTodo(id) {
			todoList.forEach((elem, index) => {
				if(elem.id === id) {
					elem.completed = !elem.completed;
				}
			});
		}
	};
}

export {todoFactory};
